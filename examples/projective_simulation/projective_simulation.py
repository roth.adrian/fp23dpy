import tqdm
import numpy as np
import matplotlib.pyplot as plt

import fp23dpy
from fp23dpy import simulation
from fp23dpy.examples import example_cone


def print_projections(name="print_projections"):
    recorded_image_shape = (512, 512)

    coordinate_grid_shape = (230, 512)
    calibration = example_cone.get_calibration()
    # extract the unprojected 3D structure
    coordinate_grid = simulation.get_rotsym_coordinate_grid(
        example_cone.radius,
        coordinate_grid_shape,
        scale=calibration["scale"],
        mid_y=example_cone.get_mid_y(),
    )

    coordinate_grid[1] -= coordinate_grid[
        1, coordinate_grid_shape[0] // 2, coordinate_grid_shape[1] // 2
    ]
    coordinate_grid = coordinate_grid.filled(np.nan)

    d_lim = 280
    diff_lim = 30
    distance_size_ratios = [100, 35, 10]
    max_cone_size = np.nanmax(coordinate_grid[2]) - np.nanmin(coordinate_grid[2])

    for i in tqdm.tqdm(range(3)):
        camera_radius = distance_size_ratios[i] * max_cone_size

        image_map, current_calibration = simulation.estimate_projective_rotsym_map(
            camera_radius,
            calibration,
            coordinate_grid,
            recorded_image_shape,
            camera_y=0,
        )
        image_map = np.ma.array(image_map, mask=np.isnan(image_map))
        image_signal = simulation.render_from_xmap(image_map[0], current_calibration)
        image_signal.fill_value = 0

        plt.figure()
        plt.title(f"distance_size_ratio = {distance_size_ratios[i]:.1f}")
        plt.imshow(image_map[2], vmin=0, vmax=d_lim)
        plt.colorbar(label="depth")
        plt.savefig(f"depth_{i}")
        plt.close()

        plt.figure()
        plt.imshow(image_signal.filled(), cmap="gray")
        plt.savefig(f"signal_{i}")
        plt.close()

        reconstruction = fp23dpy.fp23d(image_signal, current_calibration)
        plt.figure()
        plt.title(f"distance_size_ratio = {distance_size_ratios[i]:.1f}")
        plt.imshow(reconstruction[2], vmin=0, vmax=d_lim)
        plt.colorbar(label="depth")
        plt.savefig(f"reconstructed_depth_{i}")
        plt.close()

        error = reconstruction - image_map
        plt.figure()
        plt.title(f"distance_size_ratio = {distance_size_ratios[i]:.1f}")
        plt.imshow(error[2], cmap="PiYG", vmin=-diff_lim, vmax=diff_lim)
        plt.colorbar(label="error")
        plt.savefig(f"error_reconstructed_depth_{i}")
        plt.close()


if __name__ == "__main__":
    print_projections()
