"""
If segmentation of a time-series have multiple disconnected areas where the absolute
phase is not known for all areas, the temporal-alignment is a rough tool for solving it.
"""
import numpy as np
from pathlib import Path
from skimage import io
import subprocess

from fp23dpy.examples import example_cone

file_dir = Path(__file__).parent

name = "time_point"


def create_example():
    pass

    # time_point1
    calibration = example_cone.get_calibration()
    signal = 255 / 2 * example_cone.render()
    segmentation = ~signal.mask

    signal.data[signal.mask] = 0
    io.imsave("{}_1.png".format(name), signal.data.astype(np.uint8))
    io.imsave(
        "segmented_{}_1.png".format(name),
        segmentation.astype(np.uint8) * 255,
        check_contrast=False,
    )
    calibration.write("calibration_{}_1.txt".format(name))

    # time_point1
    shape = signal.shape
    signal_2 = signal.copy()
    segmentation_2 = segmentation.copy()

    # moving part of the structure
    start_position = 300
    movement = 10
    signal_2[start_position + movement :] = signal_2[start_position:-movement]
    signal_2[start_position : start_position + movement] = 0
    segmentation_2[start_position + movement :] = segmentation_2[
        start_position:-movement
    ]
    segmentation_2[start_position : start_position + movement] = False

    io.imsave("{}_2.png".format(name), signal_2.data.astype(np.uint8))
    io.imsave(
        "segmented_{}_2.png".format(name),
        segmentation_2.astype(np.uint8) * 255,
        check_contrast=False,
    )
    # required information of how the data was moved, THIS IS IMPORTANT
    mid_x = shape[1] // 2
    calibration["area_tracks"] = [
        [
            [mid_x, start_position + 3 + movement],
            [mid_x, start_position + 3],
        ]
    ]
    calibration.write("calibration_{}_2.txt".format(name))


def run_reconstruction():
    # This can also be run from the terminal
    command = f"python -m fp23dpy {name}_1.png {name}_2.png --temporal-alignment --print-image"
    print(command)
    subprocess.run(command, shell=True)


if __name__ == "__main__":
    create_example()
    run_reconstruction()
