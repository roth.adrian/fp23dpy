# Fringe Pattern to 3D python
This is a python package for applying phase demodulation and 3D reconstruction as post processing of Fringe Pattern (FP) images recorded using the Fringe Projection - Laser Induced Fluorescence (FP-LIF) technique.
The package has been developed at the division of Combustion Physics at Lund University and a more detailed explanation of the technique is found in the article cited below together with on our webpage [spray-imaging.com/fplif.html](https://spray-imaging.com/fp-lif.html).

[See showcase 3D results using this package (might take a minute to load)](https://3d.spray-imaging.com/cone4d_evolution).

For any questions and code errors please contact submit an issue at [gitlab](https://gitlab.com/roth.adrian/fp23dpy).

![Process from structure to fringe pattern to 3D recontruction](./readme_assets/process.jpg)

## Installation through pip
```
pip install git+https://gitlab.com/roth.adrian/fp23dpy.git@master
```
Or clone the repository and

```
pip install <path-to-fp23dpy>
```

## Usage
From a terminal you can use

```
python3 -m fp23dpy <FP-image>
```

that will by take the input image of a fringe pattern (and optional segmentation and calibration files) and attempt to 3D reconstruct together with the result as a GL Transmission Format file `.glb` which can be imported into most 3D viewing software, for example [threejs-editor](https://threejs.org/editor/).
Other 3D file formats are supported with `--ext <extension>` flag such as `stl` or `obj`, the package use [trimesh](https://github.com/mikedh/trimesh) so all export formats for that package are supported.
For more information on possible flags to the command file use `python -m pf23dpy -h`.

To use the package for reconstruction in your own code use:

```python
import fp23dpy
reconstruction = fp23dpy.fp23d(image, calibration)
```
See the docstring for more information.

Examples of different 3D structures can be found in the `fp23dpy/examples` folder of the source code.
To print these examples' segmentation, calibration and simulated FP-image run the following from the examples folder of the repository,

```
python print_example_structures.py
```

In the same script, the code for reconstruction is also found. You can also reconstruct by running for the example drop,

```
python -m fp23dpy example_drop.png
```

open the written `reconstructed_example_drop.glb` in a 3D viewer such as blender.

### Calibration
A calibration file can be used for each FP-LIF image, if this is not provided the program will try to calibrate using the given image which is not a robust procedure.
The calibration file name is either `calibration_<FP-image-filename>.txt` (without image type extension) or `calibration.txt` where the second will be default for the whole directory.
The file should include a JSON format object with the following attributes:

```python
{
    "T":     float,		 # describing the fringe pattern period length in pixels with an image recorded of a plane 3D object orthogonal to the fringe projection direction
    "gamma": float,		 # float describing the angle in degrees of the fringes in the image, 0 if vertical fringes and 90 if horizontal
    "theta": float,	 	 # the angle in degrees from the camera to the fringe projection direction (optional, will create wrong of output)
    "scale": float,  	 # scale of the image, number of pixels per meter (optional, will scale output to pixels otherwise)
    "phi":   float,	 	 # the rotation in degrees of the camera in spherical coordinates to the angle of the fringe pattern with a certain radius (optional)
    "Tlim":  list of floats  # suggestion of T limits to search within, will not always be respected (optional)
    "principal_point": list with length 2, [x_p, y_p]  # Connected to the principal point in camera calibration. This is usually where the center of the camera chip is but when assuming orthographic camera it can be anywhere on the image and descripes which point the theta and phi angles is rotating around (optional, center of image is used by default)
    "absolute_phase": list with length 3, [x_a, y_a, absolute_phase]  # To get real 3D values the absolute phase is required. By defining the absolute_phase at a single pixel x_a, y_a the pixels connected to this pixel will also have known absolute phase. This parameter can also be a list of multiple absolute_phase points for different areas in the mask. If you have multiple absolute_phase points in the same area, the last one will be used. (optional, if no absolute phase for unmasked area then its min value is set to zero for the third dimension)
    "absolute_threeD": list with length 3, [x_a, y_a, absolute_threeD]  # Same as absolute_phase but here you can set the threeD values of pixels instead. If both absolute keys are found in the calibration this value will be used.
}
```

The script `fp23d calibrate <calibration-image>` can be used for easier estimation of `T` and `gamma` from a calibration image.
The calibration image should be of a flat object placed with its surface facing the illumination source and imaged with clear fringes.

### Segmentation
If only parts of the image has the required Fringe Pattern lines, which is the case example drop, a segmentation of the image should be produced.
The segmented file should have filename `segmentation_<FP-image-filename>` (with image type extension).
If a single segmentation file should be used for all FP images in the same directory the segmentation file can be called `segmentation.png`.
The file should have zero values for background pixels and non-zero for foreground pixels.

There are some attempt of automated segmentation algorithms in `fp23dpy.segmentation`. None are foolproof but you can check them out to use as inspiration.

### Advanced usage

#### Temporal alignment
If you do not know the absolute_phase of some disconnected areas and have time-series data, you can use area tracks together with the `--temporal-alignment` flag of the main function. The key `area_tracks` should then be added to the calibration file for each fringe pattern that needs tracking. It should contain values for each area's previous pixel position and current position. An example of temporal alignment is found in the examples folder.

#### Square carrier
The reconstruction works under the assumption that you are using orthographic cameras. In other words cameras where objects at different distances are imaged with the same size. This is typically not the case but is a good approximation for a projective camera if the distance between the camera and the object is large compared to the size of the object. When this approximation is not sufficient, a hack is to use a so-called square carrier where that can compensate slightly for the errors. In practice this means that the value for the key `T` in the calibration is a list instead of a single value. The list are the coefficients for the 2D square function of the carrier. To estimate a square carrier the calibration as

#### Projective simulation
There is an example script for how to produce simulations of projective cameras in the examples folder. As previously mentioned, the current implementation assume orthographic cameras in the 3D reconstruction. With this simulation you can gain an understanding of how much this assumption affects the reconstruction results.

```
python -m fp23dpy calibrate --auto --square-carrier <calibration-image>`.
```


## Citation
[Adrian Roth, Elias Kristensson, and Edouard Berrocal, "Snapshot 3D reconstruction of liquid surfaces," Opt. Express 28, 17906-17922 (2020)](https://doi.org/10.1364/OE.392325)

### Bibtex
```
@article{Roth:2020,
author = {Adrian Roth and Elias Kristensson and Edouard Berrocal},
journal = {Opt. Express},
number = {12},
pages = {17906--17922},
publisher = {OSA},
title = {Snapshot 3D reconstruction of liquid surfaces},
volume = {28},
month = {Jun},
year = {2020},
url = {http://www.opticsexpress.org/abstract.cfm?URI=oe-28-12-17906},
doi = {10.1364/OE.392325},
}
```
